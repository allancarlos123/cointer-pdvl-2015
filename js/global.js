/*
	Menu lateral
*/
function menu() {
	$('.nav-toggle').click(function() {
		if($(".nav").hasClass("side-fechado")) {
			$('.nav').animate({
			    left: "0px",
			}, 100, function() {
			    $(".nav").removeClass("side-fechado");
				$('section').css('z-index', '-1000');
				$("#menu").html("[X]");
			});
		}
		else {
			$('.nav').animate({
			    left: "-230px",
			}, 100, function() {
			    $(".nav").addClass("side-fechado");
				$('section').css('z-index', '1000');
				$("#menu").html("Menu");
			});
		}
	});
}
	
//Menu Sidebar
$(document).ready(function() {
	$('section').css('z-index', '1000');
	$('.nav').css('left', '-230px').addClass('side-fechado');;
	$('.nav').append( "<button id='menu' class='btn btn-primary nav-toggle'>Menu</button>" );
	
	menu();
});

/*
	Menu laterial end
/////////////////////////////
*/